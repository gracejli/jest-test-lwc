import { createElement } from 'lwc';
import Cmm_specificGroupSharing from 'c/c-cmm-specific-group-sharing';
import getSpecificGroups from '@salesforce/apex/GroupController.getSpecificGroups';

// Mocking  Apex method call
jest.mock(
    '@salesforce/apex/GroupController.getSpecificGroups',
    () => {
        return {
            default: jest.fn()
        };
    },
    { virtual: true }
);

// Sample data for Apex call
const APEX_GROUPS_SUCCESS = [
    {
        "Id": "000001112222DDDD001",
        "UserOrGroupId":  "00AAABBBCCCDDD12345", 
        "Name":  "Asia Pacific"
    }
];

describe('c-cmm-specific-group-sharing', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
        // Prevent data saved on mocks from leaking between tests
        jest.clearAllMocks();
    });

    // Helper function to wait until the microtask queue is empty. This is needed for promise
    // timing when calling imperative Apex.
    function flushPromises() {
        // eslint-disable-next-line no-undef
        return new Promise(resolve => setImmediate(resolve));
    }

   it('passes the recordId to the Apex method correctly', () => {
        const RECORD_ID = '00AAABBBCCCDDD12345';
        const APEX_PARAMETERS = { recordId: RECORD_ID };

        // Assign mock value for resolved Apex promise
        getSpecificGroups.mockResolvedValue(APEX_GROUPS_SUCCESS);

        // Create initial element
        const element = createElement('c-cmm-specific-group-sharing', {
            is: Cmm_specificGroupSharing
        });
        element.recordId = RECORD_ID;
        document.body.appendChild(element);

        // Select button for executing Apex call
        const buttonEl = element.shadowRoot.querySelector('lightning-button');
        buttonEl.click();

        return flushPromises().then(() => {
            // Validate parameters of mocked Apex call
            expect(getSpecificGroups.mock.calls[0][0]).toEqual(APEX_PARAMETERS);
        });
    });

    it('renders one sharing group', () => {
    
        // Assign mock value for resolved Apex promise
        getSpecificGroups.mockResolvedValue(APEX_GROUPS_SUCCESS);

        // Create initial element
        const element = createElement('c-cmm-specific-group-sharing', {
            is: Cmm_specificGroupSharing
        });
        document.body.appendChild(element);

     
        // Select button for executing Apex call
        const buttonEl = element.shadowRoot.querySelector('lightning-button');
        buttonEl.click();

        return flushPromises().then(() => {
      
            const detailEls = element.shadowRoot.querySelectorAll('lightning-pill');
            expect(detailEls.length).toBe(APEX_GROUPS_SUCCESS.length);
            expect(detailEls[0].label).toBe(
                APEX_GROUPS_SUCCESS[0].Name
            );
        });
    });

    it('handleRemoveSelectedItem works', () => {
        // Create element
        const element = createElement('c-cmm-specific-group-sharing', {
            is: Cmm_specificGroupSharing
        });
        element.availableGroups = APEX_GROUPS_SUCCESS;
        document.body.appendChild(element);
    
        // Remove a selected item
        const selPills = element.shadowRoot.querySelectorAll('lightning-pill');
        selPills[0].dispatchEvent(new CustomEvent('remove'));
        // Check selection
        expect(element.availableGroups.length).toBe(0);
    });
});
